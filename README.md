# Projet SQL - Training Center (Centre de formation) 

L'objectif du projet sera de faire en groupe la conception du modèle de donnée, la mise en place de la base de données et les composants d'accès aux données pour une application de gestion d'un centre de formation.

## Étapes du projet
1. Identifier et lister sous forme de user stories ou de diagramme de use case les fonctionnalités de l'application
2. (Optionnel) Créer les maquettes fonctionnelles de quelques écrans de l'application
3. Identifier et schématiser sous forme de diagramme de classe les entités qui persisteront en base de données pour que ces fonctionnalités soient possibles
4. Créer un ou des scripts SQL qui permettront de mettre en place la base de données, ils contiendront la création des tables et l'insertion d'un jeu de données
5. Dans un projet Java, créer les classes représentant les entités et les composant d'accès aux données en utilisant JDBC

## Organisation conseillée
* La phase de conception, d'identification des fonctionnalités et des classes est bien à faire de manière collégiale, sur un pc ou sur papier en faisant en sorte que tout le monde puisse s'exprimer
* Créer dès le début un projet Gitlab afin de pouvoir utiliser les outils de ce dernier : les issues pour lister les fonctionnalités et tâche à réaliser
* Réaliser les diagrammes soit sur papier, soit sur [staruml](https://staruml.io/), ou bien sur [draw.io](https://app.diagrams.net/) ou [lucidchart](https://www.lucidchart.com/) (mais ces deux derniers sont un peu moins agréables à utiliser)
* Lorsque vous commencez à travailler sur des tâches différentes, commencer chaque après midi par un standing meeting afin que chaque membre du groupe sache ses propres tâches et celles des autres
* Créer des scripts SQL séparés pour chaque table+insertion de données afin de plus facilement vous répartir le travail

## Rendu Attendu
Un dépôt gitlab avec un projet Java contenant : 
* Un README.md avec les diagrammes de Use Case et de Class un peu commenté (fonctionnalités principales, relations intéressantes entre les entités, etc.)
* Les fichiers SQL contenant au moins la création des tables, l'insertion du jeu de données, et pourquoi pas les requêtes identifiées pour certaines des fonctionnalités
* les différentes classes entités et repositories 

## Les groupes
* Jason, Jim, Henrique
* Sérigne, Amine, Tarik, Camille A 
* Mustapha, Damien, Aurélien
* Mohammad, Romain , Camille T, Safik
